from random import randint
import requests, json
from datetime import datetime
import time
import mysql.connector
from mysql.connector import Error

#get current weather data
def weartherAPI(city):
	url = "https://community-open-weather-map.p.rapidapi.com/weather"

	querystring = {"q":f"{city}"}

	headers = {
		"X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
		"X-RapidAPI-Key": "aa55fb9bcfmsh738ba3ba4a8b5a8p178c27jsn85f8f533ea0d"
	}

	response = requests.request("GET", url, headers=headers, params=querystring).json() #current weather data save as a json file
	return response


def connectMySQL(host, userName, pw):
	try:
		conn = mysql.connector.connect(host=f"{host}",
										user=f"{userName}",
										password=f"{pw}")
		if conn.is_connected():
			print("connected")
			return conn
	except Error as e:
		print("Error",e)


def createDB(cursor, db_name):
	cursor.execute(f"CREATE DATABASE IF NOT EXISTS {db_name};")


def createTable(cursor, db_name, table_info):
	# Create table without assign attributes
	table_name = table_info['name']
	cursor.execute(f'''
		USE {db_name};
	''')

	cursor.execute(f'''
		CREATE TABLE IF NOT EXISTS {table_name}(test int);
	''')

	# assign attributes to table
	for attr in table_info['attributes']:
		column_name = attr['label']
		data_type = attr['type']
		if 'constraint' in attr:
			constraint = ' '.join(attr['constraint'])
		else:
			constraint = ''

		cursor.execute(f'''
			USE {db_name};
		''')

		cursor.execute(f'''
			ALTER TABLE {table_name}
			ADD {column_name} {data_type} {constraint};
		''')

	#Set primary key for Fact table
	if 'primary_keys' in table_info:
		pks = table_info['primary_keys']
		cursor.execute(f'''
				USE {db_name};
		''')

		cursor.execute(f'''
			ALTER TABLE {table_name}
			ADD PRIMARY KEY ({','.join([key for key in pks])});
		''')

	# Set foreign key
	if 'foreign_keys' in table_info:
		fks = table_info['foreign_keys']

		for fk in fks:
			cursor.execute(f'''
				USE {db_name};
			''')

			cursor.execute(f'''
				ALTER TABLE {table_name}
				ADD FOREIGN KEY ({fk['foreign_key']}) REFERENCES {fk['reference_table']}({fk['reference_attribute']});
			''')

	cursor.execute(f'''
		USE {db_name};
	''')

	cursor.execute(f'''
				ALTER TABLE {table_name}
					DROP COLUMN test;''')


#Transform utc to timestamp
def utcToTime(response):
	return datetime.utcfromtimestamp(response)


#Set null values is 0
def nullValues(response, attr):
	if attr in response['main']:
		return response['main'][attr]
	elif attr in response:
		return response[attr]['1h']
	elif attr in response['wind']:
		return response['wind'][attr]
	return 0


def addCity(response, cursor):
	raw_data = [ response['id'],
				response['name'], 
				response['coord']['lon'], 
				response['coord']['lat']]

	cursor.execute(f'''
		USE CurrentWeatherAPI;
	''')
	try:
		cursor.execute("INSERT INTO City VALUES (%s,%s,%s,%s);", raw_data)
	except:
		print("Can't")


def addWeather(response, cursor):
	for i in range(len(response['weather'])):
		raw_data = [response['weather'][i]['id'],
					response['weather'][i]['main'], 
					response['weather'][i]['description'], 
					response['weather'][i]['icon']]
		cursor.execute(f'''
			USE CurrentWeatherAPI;
		''')
		try:
			cursor.execute("INSERT INTO Weather VALUES (%s,%s,%s,%s);", raw_data)
		except:
			print("Can't")


def addStation(response, cursor):
	sunrise = utcToTime(response['sys']['sunrise'])
	sunset = utcToTime(response['sys']['sunset'])
	raw_data = [response['sys']['id'], 
				response['sys']['country'], 
				response['sys']['type'], 
				sunrise, 
				sunset, 
				response['timezone']]
	cursor.execute(f'''
		USE CurrentWeatherAPI;
	''')
	try:
		cursor.execute("INSERT INTO Station VALUES (%s,%s,%s,%s,%s,%s);", raw_data)
	except:
		print("Can't")


def addCurrentWeatherFact(response, cursor):
	sea_level = nullValues(response, 'sea_level')
	grnd_level = nullValues(response, 'grnd_level')
	wind_gust = nullValues(response,'gust')
	rain = nullValues(response, 'rain')
	snow = nullValues(response, 'snow')
	response['dt'] = randint(1000000,1000000000)
	date_time = utcToTime(response['dt'])

	raw_data  =[response['id'],
				response['weather'][0]['id'], 
				response['sys']['id'], 
				date_time,
				response['main']['temp'], 
				response['main']['feels_like'], 
				response['main']['temp_min'], 
				response['main']['temp_max'], 
				response['main']['pressure'], 
				response['main']['humidity'],
				sea_level,
				grnd_level,
				response['visibility'],
				response['wind']['speed'],
				response['wind']['deg'],
				wind_gust,
				response['clouds']['all'],	
				rain,
				snow]
	
	cursor.execute(f'''
		USE CurrentWeatherAPI;
	''')
	print("INSERT INTO CurrentWeatherFact VALUES (%s,%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);" % tuple(raw_data))
	cursor.execute("INSERT INTO CurrentWeatherFact VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);", raw_data)
	# try:
	# 	# cursor.execute("INSERT INTO CurrentWeatherFact VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);", raw_data)
	# except:
	# 	print("Can't")


def pipeline(conn):
	print('Calling API')
	cursor = conn.cursor()
	response = weartherAPI("London")
	if response['cod'] == 200:
		addCity(response, cursor)
		print("added City")
		addStation(response, cursor)
		print("added Station")
		addWeather(response, cursor)
		print("added Weather")
		addCurrentWeatherFact(response, cursor)
		print("added Fact")
		conn.commit()
	print('End')


def main():
	with open('db_info.json') as f:
		db_info = json.load(f)

	conn = connectMySQL("mysql", "root", "root")
	cursor = conn.cursor()

	createDB(cursor, db_info['name'])
	conn.commit()
	print("DB created")

	for table in db_info['tables']:
		table_name = table['name']
		createTable(cursor, db_info['name'], table)
		conn.commit()
		print(f"Table {table_name} created")

	#run every 1 minute
	while True:
		pipeline(conn)
		time.sleep(60)

# main()
