import ETL
import json
from datetime import datetime
import unittest
import mysql.connector

class TestETL(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.conn =mysql.connector.connect(host="mysql",
										user="root",
										password="root")
        cls.cursor = cls.conn.cursor()
        with open('db_info.json') as f:
            cls.db_info = json.load(f)
        db_name = cls.db_info['name']
        ETL.createDB(cls.cursor, db_name)
        cls.conn.commit()
        for table in cls.db_info['tables']:
            ETL.createTable(cls.cursor, db_name, table)
            cls.conn.commit()

        
    def setUp(self):
        with open('testcase1.json') as f:
            self.response = json.load(f)


    #test if database exists or not
    def test_createDB(self):
        print("test_createDB")
        database_name = 'CurrentWeatherAPI'
        TestETL.cursor.execute("SHOW databases like %s;",(database_name,))
        data = TestETL.cursor.fetchall()
        self.assertNotEqual(len(data), 0)
    
    #test number of tables have in database
    def test_createTable(self):
        print("test_createTable")
        for table in TestETL.db_info['tables']:
            table_name = table['name']
            TestETL.cursor.execute("SHOW tables like %s;", (table_name,))
            data = TestETL.cursor.fetchall()
            self.assertEqual(len(data),1)


    def test_addCity(self):
        print("test_addCity")
        ETL.addCity(self.response, TestETL.cursor)
        TestETL.conn.commit()
        raw_data = [
            self.response['id'],
            self.response['name'], 
            self.response['coord']['lon'], 
            self.response['coord']['lat']
        ]

        TestETL.cursor.execute(f'''
            SELECT * FROM City
            WHERE city_ID = {raw_data[0]} AND
                city_Name = '{raw_data[1]}' AND
                lon = {raw_data[2]} AND
                lat = {raw_data[3]}
        ''')
        data = TestETL.cursor.fetchall()
        self.assertEqual(len(data), 1)

    def test_addWeather(self):
        print("test_addWeather")
        ETL.addWeather(self.response, TestETL.cursor)
        TestETL.conn.commit()
        for i in range(len(self.response['weather'])):
            raw_data = [
                self.response['weather'][i]['id'],
			    self.response['weather'][i]['main'], 
			    self.response['weather'][i]['description'], 
			    self.response['weather'][i]['icon']
            ]
            TestETL.cursor.execute('''
                    SELECT * 
                    FROM Weather
                        WHERE weather_ID = %s
            ''', (raw_data[0],))
            data = TestETL.cursor.fetchall()
            self.assertEqual(len(data), 1)
        
    def test_addStation(self):
        print("test_addStation")
        ETL.addStation(self.response, TestETL.cursor)
        TestETL.conn.commit()
        sunrise = datetime.utcfromtimestamp(self.response['sys']['sunrise'])
        sunset = datetime.utcfromtimestamp(self.response['sys']['sunset'])
        raw_data = [self.response['sys']['id'],
				    self.response['sys']['id'], 
				    self.response['sys']['country'], 
				    self.response['sys']['type'], 
				    sunrise, 
				    sunset, 
				    self.response['timezone']]
        
        TestETL.cursor.execute('''
            SELECT *
            FROM Station
            WHERE station_ID = %s
        ''', (raw_data[0],))
        data = TestETL.cursor.fetchall()
        self.assertEqual(len(data),1)

    # def test_addCurrentWeatherFact(self):
    #     ETL.addCurrentWeatherFact(self.response, TestETL.cursor)
    #     TestETL.conn.commit()
    #     date_time = datetime.utcfromtimestamp(self.response['dt'])
    #     raw_data=[self.response['id'],
    #             self.response['weather'][0]['id'], 
	# 			self.response['sys']['id'],
    #             date_time]
    #     TestETL.cursor.execute('''
    #         SELECT * 
    #         FROM CurrentWeatherFact
    #         WHERE city_ID = %s AND
    #             weather_ID = %s AND
    #             station_ID = %s AND
    #             date_time = %s
    #     ''', raw_data)
    #     data = TestETL.cursor.fetchall()
    #     self.assertEqual(len(data),1)

if __name__ == '__main__':
    unittest.main()